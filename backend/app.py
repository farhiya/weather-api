from flask import Flask
import requests
import random

app = Flask(__name__)
  
@app.route('/', methods =['GET'])
def home():
    url = "https://api.openweathermap.org/data/2.5/weather?q=London&appid=65e4e3c4a4e65049b63833e049ce0702"
    response = requests.get(url)

    list_of_data = response.json()
    
    html_data = f"""
    <table border="1">
    <tr>
        <td>city</td>
        <td>coordinate</td>
        <td>temp</td>
        # <td>description</td>
        <td>pressure</td>
        <td>humidity</td>
    </tr>
    <tr>
        <td>{str(list_of_data['name'])}</td>
        <td>{str(list_of_data['coord']['lon']) + ' ' 
                    + str(list_of_data['coord']['lat'])}</td>
        <td>{str(list_of_data['main']['temp']) + 'k'}</td>
        <td>{str(list_of_data['weather'][0]['description'])}</td>
        <td>{str(list_of_data['main']['pressure'])}</td>
        <td>{str(list_of_data['main']['humidity'])}</td>
    </tr>

</table>
    """
    # return list_of_data
    return html_data


# @app.route('/city')
# def search_city():
#     API_KEY = '65e4e3c4a4e65049b63833e049ce0702'  # initialize your key here
#     cities= ["london", "lisbon", "burao", "kuala lumpur", "kyoto", "madrid", "cairo", "vancouver", 'jhb']
#     city = random.choice(cities)
    
#     # call API and convert response into Python dictionary
#     url = f'http://api.openweathermap.org/data/2.5/weather?q={city}&APPID={API_KEY}'
#     response = requests.get(url).json()
    
#     # error like unknown city name, inavalid api key
#     if response.get('cod') != 200:
#         message = response.get('message', '')
#         return f'Error getting temperature for {city.title()}. Error message = {message}'
    
#     # get current temperature and convert it into Celsius
#     current_temperature = response.get('main', {}).get('temp')
#     if current_temperature:
#         current_temperature_celsius = round(current_temperature - 273.15, 2)
#         return f'Current temperature of {city.title()} is {current_temperature_celsius} ºC'
#     else:
#         return f'Error getting temperature for {city.title()}'


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
    # app.run(debug=True)